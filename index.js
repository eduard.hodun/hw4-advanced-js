const filmList = document.querySelector(".characters");
const spinner = document.querySelector('.spinner');


spinner.innerText = 'Loading...'; 

fetch('https://ajax.test-danit.com/api/swapi/films')
    .then(res => {
        if (!res.ok) {
    throw new Error("Відповідь мережі була не ok.");
  }
        return res.json();
    })
    .then(films => {

        
        
        console.log(films);

        films.forEach(film => {
            const liItem = document.createElement("li");
            
            
            liItem.textContent = film.name;

            const spanEpisodeId = document.createElement("p");

            spanEpisodeId.textContent = film.episodeId;

            const spanOpeningCrawl = document.createElement("p");

            spanOpeningCrawl.textContent = film.openingCrawl;
            spanOpeningCrawl.classList.add('span');

            liItem.append(spanEpisodeId);
            liItem.append(spanOpeningCrawl);
            
            // liItem.insertAdjacentElement("beforeend",
            //     `<h1>${film.name} </h1>
            //     <span>${film.episodeId}</span>
            //     <span>${film.openingCrawl}</span>`);

            filmList.classList.add("characters");
            filmList.append(liItem);

             

            film.characters.forEach(actorUrl => {
                

                console.log(actorUrl);

                fetch(actorUrl)
                .then(res => res.json())
                .then(dataCharacters => {
                
                    console.log(dataCharacters);
                    spinner.innerText = '';

                        console.log(dataCharacters.name);

                         const p = document.createElement('p');
                         p.textContent = dataCharacters.name;
                         p.classList.add("actors");
                         liItem.insertAdjacentElement("beforeend", p);

                    
                });
    


            })
            
            


            
            
        });
    })
    .catch(err => {
        console.log("ERROR:", err.name); // "TypeError"
        
    });



 
